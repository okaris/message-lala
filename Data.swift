//
//  Data.swift
//  MessageLaLa
//
//  Created by Ömer Karışman on 09/07/2017.
//  Copyright © 2017 okaris. All rights reserved.
//

import Foundation
import Alamofire
import JSONHelper

class Data {
    static let sharedInstance = Data()
    
    let defaults = UserDefaults.standard
    
    var messages:[Message]? = []
    var currentUser:User?
    private init() {
        
    }
    func getHistory(){
        Alamofire.request("https://jsonblob.com/api/jsonBlob/3cf871b2-f7cd-11e6-95c2-115605632e53").responseJSON { response in
            if let json = response.result.value as? NSDictionary {
                self.messages <-- json["messages"]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "historyLoaded"), object: nil)
            }
        }
    }
    func setUser(nickname:String){
        self.currentUser = User(dictionary: ["nickname": nickname, "avatarUrl": "http://www.teequilla.com/images/tq/empty-avatar.png"])
        defaults.set(self.currentUser?.nickname, forKey: "currentUser")
    }
    func getUser() -> User?{
        if(self.currentUser == nil){
            if let storedNickname = defaults.object(forKey: "currentUser") {
                self.currentUser = User(dictionary: ["nickname": storedNickname, "avatarUrl": "http://www.teequilla.com/images/tq/empty-avatar.png"])
            }
        }
        return self.currentUser
    }
    func addOutgoingMessage(text: String){
        var newMessage = Message(dictionary: ["text": text, "timestamp": Date().toMillis(), "outgoing": true])
        newMessage.user = self.currentUser
        self.messages?.append(newMessage)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newMessages"), object: nil)
    }
}

extension Date {
    func toMillis() -> Double! {
        return Double(self.timeIntervalSince1970)
    }
}

class User: Deserializable {
    var id: Int?
    var avatarUrl: String?
    var nickname: String?
    
    required init(dictionary: [String : Any]) {
        id <-- dictionary["id"]
        avatarUrl <-- dictionary["avatarUrl"]
        nickname <-- dictionary["nickname"]
    }
}

class Message: Deserializable {
    var id: Int?
    var text: String?
    var timestamp: Double?
    var user: User?
    var outgoing: Bool = false
    
    required init(dictionary: [String : Any]) {
        id <-- dictionary["id"]
        text <-- dictionary["text"]
        timestamp <-- dictionary["timestamp"]
        user <-- dictionary["user"]
        outgoing <-- dictionary["outgoing"]
    }
}
