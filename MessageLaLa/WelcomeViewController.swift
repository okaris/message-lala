//
//  WelcomeViewController.swift
//  
//
//  Created by Ömer Karışman on 09/07/2017.
//
//

import Foundation
import UIKit

class WelcomeViewController: UIViewController {
    @IBOutlet weak var nicknameInput:UITextField?
    @IBOutlet weak var enterButton:UIButton?

    var welcomed: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Data.sharedInstance.getHistory()
        
        enterButton?.isEnabled = false
        enterButton?.backgroundColor = UIColor.init(colorLiteralRed: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        
        if((Data.sharedInstance.getUser()) != nil && !welcomed){
            welcomed = true
            let messagingController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "messagesViewController")

            self.navigationController?.pushViewController(messagingController, animated: false)
        }
        print(Data.sharedInstance.getUser()?.nickname as Any)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        nicknameInput?.becomeFirstResponder()
    }
    
    @IBAction func saveNickname(){
        Data.sharedInstance.setUser(nickname: (nicknameInput?.text)!)
        let messagingController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "messagesViewController")

        self.navigationController?.pushViewController(messagingController, animated: true)
    }
    
    @IBAction func textChanged(sender: UITextField){
        if((sender.text?.characters.count)! > 2){
            enterButton?.isEnabled = true
            enterButton?.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 122/255, blue: 1, alpha: 1)
        }else{
            enterButton?.isEnabled = false
            enterButton?.backgroundColor = UIColor.init(colorLiteralRed: 0.8, green: 0.8, blue: 0.8, alpha: 1)

        }
    }
}
