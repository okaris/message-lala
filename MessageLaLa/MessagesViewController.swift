//
//  MessagesViewController.swift
//  MessageLaLa
//
//  Created by Ömer Karışman on 09/07/2017.
//  Copyright © 2017 okaris. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

class MessagesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    @IBOutlet weak var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.backItem?.title = "Leave"

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.newMessagesArrived), name: NSNotification.Name(rawValue: "newMessages"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.historyLoaded), name: NSNotification.Name(rawValue: "historyLoaded"), object: nil)

        self.tableView?.rowHeight = UITableViewAutomaticDimension
        self.tableView?.estimatedRowHeight = 50
        self.tableView?.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView?.contentInset = UIEdgeInsetsMake(0, 0, 12, 0)
        self.title = Data.sharedInstance.currentUser?.nickname
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func historyLoaded() {
        self.tableView?.reloadData()
    }
    
    func newMessagesArrived() {
        self.tableView?.beginUpdates()
        
        let indexPath:IndexPath = IndexPath(row:(Data.sharedInstance.messages!.count - 1), section:0)
        self.tableView?.insertRows(at: [indexPath], with: .right)
        
        self.tableView?.endUpdates()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.tableView?.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }

    }
    
    func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: {
                            self.view.layoutIfNeeded()
                            let indexPath:IndexPath = IndexPath(row:(Data.sharedInstance.messages!.count - 1), section:0)
                            self.tableView?.scrollToRow(at: indexPath, at: .bottom, animated: false)
                            },
                           completion: nil)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (Data.sharedInstance.messages?.count)!
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: messageCell;
        
        let message = Data.sharedInstance.messages?[indexPath.row]
        if(message?.outgoing)!{
            cell = tableView.dequeueReusableCell(withIdentifier: "messageCellOutgoing") as! messageCell
        }else{
            cell = tableView.dequeueReusableCell(withIdentifier: "messageCellIncoming") as! messageCell
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.messageLabel?.text = message?.text
        
        let date = NSDate(timeIntervalSince1970: (message?.timestamp)!)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM dd YYYY hh:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        
        cell.dateLabel?.text = dateString
        
        let url = URL(string: (message?.user?.avatarUrl)!)
        
        cell.profilePic?.af_setImage(withURL: url!)
        
        cell.nameLabel?.text = message?.user?.nickname!

        return cell
    }
    
    @IBAction func sendMessage(sender: UITextField){
        let messageText = sender.text
        Data.sharedInstance.addOutgoingMessage(text: messageText!)
        sender.text = ""
    }
}

class messageCell: UITableViewCell
{
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var messageLabel: UILabel?
    @IBOutlet weak var dateLabel: UILabel?
    @IBOutlet weak var profilePic: UIImageView?


}
